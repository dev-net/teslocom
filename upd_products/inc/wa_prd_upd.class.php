<?php
class WAPrdUpd{
	
	protected $_price       = false;
	protected $_products    = array();
	protected $_updOpts     = array(
					'Price'     => true,
					'enabled'   => true,
					'available' => true,
					'in_stock'  => true
							  );
	protected $_stats = array(
					'all'      => 0,
					'process'  => 0,
					'matched'  => 0,
					'changed'  => 0
	                    );
//	protected $_error = "";
	protected $_errors = array();
		
	public function __construct(){	
		//$this->writeLog('������ ��������');
	}
	
	public function __destruct(){
		$this->saveStats();
		$this->saveMail();
	}
	
	public function setUpdOpts($opts){
		$this->_updOpts = $opts;
	}

	public function prepareUpd()
	{
		WASSDB::connectDB();
		//WASSDB::clearAll();
	}

	public function loadPrice($url, $login = false, $pass = false, $num  = 0){

		$this->_products = array();
		$fileName = TMP_DIR.'/yml_price'.$num;
		if(file_exists($fileName) && (time() - 1800) < filemtime($fileName)){
			$this->_price = file_get_contents($fileName);
			return true;
		}
		
		$ch = curl_init();
		if (!$ch){ 
			$this->_errors[] = "���������� ��������� Curl. ���������, ���������� � ��������������.";
			return false;
		}
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		if($login&&$pass){
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ; 
			curl_setopt($ch, CURLOPT_USERPWD, $login.":".$pass); 
		}
		$result = curl_exec($ch);
		
		if (curl_errno($ch)!=0) { 
			$curl_errno = curl_errno($ch);
			$curl_error = curl_error($ch);
			curl_close($ch);
			$this->_errors[] = 'CURL error #'.$curl_errno.': '.$curl_error.' URL:'.$url;
			return false;
		}else{
			curl_close($ch);
			$this->_price = $result;
			file_put_contents($fileName, $this->_price);
			return true;
		}
	}
	
	public function parsePrice(){
		return $this->parseYML();
	}
	
	protected function parseYML(){
		if(empty($this->_price)){
			return false;
		}
		
		try
		{
			$xml = new SimpleXMLElement($this->_price);
		}
		catch (Exception $e) 
		{
			$this->_errors[] = "������ ������ XML:" + $e->getMessage();
			$this->writeLog("������ ������ XML");
			$this->writeLog("_error = ".end($this->_errors));
			return false;
		}
		foreach ($xml->shop->offers->offer as $offer) {
			$available = ((string)$offer['available']=='false')?0:1; 
			$this->_products[] = array(
									'code'      => (string)$offer->vendorCode,
									'price'     => (string)$offer->price,
									'available' => $available,
							   );
		}
		$xml = new SimpleXMLElement("<data></data>");
		unset($xml);
		return true;
	}
	
	public function updProducts(){
		if(empty($this->_products)){
			$this->parsePrice();
		}
		$c_products = count($this->_products);
		if(!$c_products){
			$this->_errors[] = "������ �����������";
			return false;
		}
		$this->_stats['all'] += $c_products;
		
		//print_r($this->_products);
		
//		WASSDB::connectDB();
		foreach($this->_products as $product){
			if(!empty($product['code'])){
				list($c_matched, $c_changed) = WASSDB::updateProduct($product, $this->_updOpts);
				$this->_stats['process']++;
				$this->_stats['matched'] += $c_matched;
				$this->_stats['changed'] += $c_changed;
			}
		}
		return true;
	}

	public function getProcessStat()
	{
		return $this->_stats['process'];
	}

	public function getMatchedStat()
	{
		return $this->_stats['matched'];
	}

	public function getChangedStat()
	{
		$this->_stats['changed'];
	}
	
	public function getError()
	{
		return $this->_errors;
	}
	
	protected function writeLog($msg = '', $status = 1){
		$status_str = ($status)? 'OK' : 'ERR';
		$log_str = sprintf("[%8s]  %-5s%s\r\n", date("H:i:s"), $status_str, $msg);
		error_log($log_str, 3, LOG_FILE);
	}
	
	protected function saveStats(){
		$stats_file = ROOT_DIR.'/stats.txt';
		if(!file_exists($stats_file)){
			$log_str = sprintf("%-20s%-15s%-15s%-15s%-15s\r\n", '����', '����� �������', '����������', '������� � ��', '������.');
			error_log($log_str, 3, $stats_file);
		}
		$log_str = sprintf("%-20s%-15s%-15s%-15s%-15s\r\n", date("Y-m-d H:i:s"), $this->_stats['all'],     $this->_stats['process'],
		                                                                         $this->_stats['matched'], $this->_stats['changed']);
		error_log($log_str, 3, $stats_file);
		unset($log_str);
	}

	protected function saveMail()
	{
		$mailText = "<b>�������� ������ �����-������ � ������� Teslocom.ru</b><br/><hr><br/><b>������� ������� ��������!</b><br/>";
		$mailText .= "���������� ".$this->_stats['process']." ����� �����-������.<br/>";
		$mailText .= "�� ��� ������� ".$this->_stats['matched']." �����.<br/>";
		$mailText .= "���� ������� �������� ".$this->_stats['changed']." �����.<br/>";

		if (count($this->_errors))
		{
			$mailText .= "� �������� ������� �������� ��������� ������: <br/>" . implode('<br />', $this->_errors);
		}

		file_put_contents(TMP_DIR . '/mail.tmp', $mailText);
	}

}
?>