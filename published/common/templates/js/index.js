function windowLoaded() {
	resizeBodyFrame();
}

function resizeBodyFrame() {
	var documentSize = getDocumentSize();
	var currentPos = getMenuPos();
	
	var bodyFrame = document.getElementById("body-frame");
	var menuBlock = document.getElementById("menu-block");
	var messagesBlock = document.getElementById("messages-block");
	var logoBlock = document.getElementById("logo-block");
	var topLine = document.getElementById("top-line");
	
	var heightOffset = topLine.offsetHeight;
	//if (logoBlock)
		//heightOffset += logoBlock.offsetHeight;
		
	//var isIE = window.navigator.appName == "Microsoft Internet Explorer";
	
	var frameMissWidth = getStyle(bodyFrame, "margin-left", true) + getStyle(bodyFrame, "margin-right", true);
	var frameMissHeight = getStyle(bodyFrame, "margin-bottom", true) + getStyle(bodyFrame, "margin-top", true);	
	
	switch (currentPos) {
		case "topmenu": case "bottommenu":
			menuBlock.style.height = "auto";
			bodyFrame.style.height = (documentSize.height - menuBlock.offsetHeight - heightOffset - messagesBlock.offsetHeight - frameMissHeight) + "px";
			bodyFrame.style.width = (documentSize.width - frameMissWidth) + "px";
			break;		
		case "leftmenu":
		case "rightmenu":
			var leftHeight = documentSize.height - heightOffset;
			menuBlock.style.height = leftHeight + "px";
			bodyFrame.style.height = (leftHeight - frameMissHeight) + "px";
			bodyFrame.style.width = (documentSize.width - menuBlock.offsetWidth  - frameMissWidth) + "px";
			break;		
	}
	
	var bodyTopRightBlock = document.getElementById("body-top-right-block");
	bodyTopRightBlock.style.top = (bodyFrame.offsetTop - getStyle(bodyFrame, "margin-top", true) )+"px";
	bodyTopRightBlock.style.right = (documentSize.width - bodyFrame.offsetWidth-bodyFrame.offsetLeft) + "px";
}

function resizeLogo () {
		//var menuPos = getMenuPos();
	var logo = document.getElementById("logo");
	if (!logo)
		return;
	logo.style.height = "auto";
	//if (logo.offsetHeight > 75)
		//logo.style.height = 75 + "px";
	/*var menuBlock = document.getElementById("menu-block");
	switch (menuPos) {
		case "topmenu": case "bottommenu":
			logo.style.height = "0px";
			logo.style.height = (menuBlock.offsetHeight -4)+ "px";
			logo.style.width = "auto";
			break;
		case "leftmenu": case "rightmenu":
			logo.style.width = "0px";
			logo.style.width = (menuBlock.offsetWidth)+ "px";
			logo.style.height = "auto";
			break;	
	}*/
}


function setFullscreen(value) {
	document.fullscreen = value;	
	setCookie("fullscreen", value);
	changeBodyClass();
}

function getFullscreen() {
	if (!document.fullscreen)
		document.fullscreen = getCookie("fullscreen");
	if (!document.fullscreen)
		document.fullscreen = false;
	return document.fullscreen;
}

function changeBodyClass() {
	var menuPos = getMenuPos();
	var menuType = getMenuType();
	var fullscreen = getFullscreen();
	var fullscreenClass = (fullscreen == "on") ? "minimized" : "normal";
	
	document.body.className = menuPos + " " + menuType + " " + fullscreenClass;	
	
	
	setTimeout("resizeBodyFrame()", 30);
	//setTimeout("resizeLogo()", 50);
}

var selectedLink;
var selectedIcon;
function openLink(link,appId, href) {
	var appData = (appId) ? document.appsData[appId] : null;
	
	var menuBlock = document.getElementById("menu-block");
	
	if (selectedLink)
		selectedLink.className = selectedLink.className.replace("selected", "unselect");
	if (link) {
		link.className = link.className.replace("unselect", "selected");
	}
	if (selectedIcon)
		selectedIcon.src = selectedIcon.src.replace("_selected", "");
	
	selectedIcon = document.getElementById("app_icon_" + appId);
	if (selectedIcon)
		selectedIcon.src = selectedIcon.src.replace(".gif", "_selected.gif");
	
	menuBlock.className = (appId) ? "menu-block for-" + appId : "menu-block";
	
	if (appData) {
		document.title = appData.name;
	}
	
	showAppMessages("NULL");
	document.linkLoadingApp = appId;
	showHideLoading(true);
	document.getElementById("body-frame").src = (href) ? href : appData.url;
	selectedLink = link;
	return false;
}

function showAppMessages(appId) {
	var closedMessagesStr = getCookie("closedMessagesIds");
	var closedMessages = (closedMessagesStr == null) ? [] : closedMessagesStr.split(",");
	if (closedMessages.indexOf == null)
		closedMessages.indexOf = arrIndexOf;
	
	var hideAllMessages = true;
	if (document.messages) {
		for (var messageId in document.messages) {
			var messageDom = document.getElementById(messageId);
			if(typeof(document.messages[messageId]) === "function")
				continue;
			if (!messageDom || (closedMessages.indexOf(messageId) != -1))
				continue;
			var finded = false;
			for (var i = 0; i < document.messages[messageId].length; i++)
				if(document.messages[messageId][i] == appId)
					finded = true;
			if (finded)
				hideAllMessages = false;
			messageDom.style.display = (finded) ? "block" : "none";
		}
	}
	var messagesBlock = document.getElementById("messages-block");
	if (hideAllMessages) {
		messagesBlock.style.height = "1px";
		messagesBlock.style.visibility = "hidden";
	} else {
		messagesBlock.style.height = "auto";
		messagesBlock.style.visibility = "visible";
	}
	//.style.display = (hideAllMessages) ? "none" : "block";
	setTimeout("resizeBodyFrame()", 30);
	//resizeBodyFrame();
}

function arrIndexOf(val) {
	for (var i in this) {
		if (this[i] == val)
			return i;
	}
	return -1;
}

function linkLoaded() {
	document.getElementById('body-frame').contentWindow.document.body.onclick = hideViewSelector;
	if (document.linkLoadingApp)
		showAppMessages(document.linkLoadingApp);
	showHideLoading(false);
	hideViewSelector();
}

function showHideLoading(show) {
	document.getElementById("loading-block").style.visibility = (show) ? "visible" : "hidden";
}

function windowResized() {
	resizeBodyFrame();
}

function highlight (appId, block) {
	if(block.className.indexOf("highlight") == -1)
		block.className += " highlight";
}

function highlightOff (appId, block) {
	if(block.className.indexOf("highlight") != -1)
		block.className = block.className.replace("highlight", "");
}

function getMenuPos() {
	if (!document.menuPos)
		document.menuPos = getCookie("menu_pos");
	if (!document.menuPos)
		document.menuPos = "topmenu";
	return document.menuPos;
}

function getMenuType() {
	if (!document.menuType)
		document.menuType = getCookie("menu_type");
	if (!document.menuType)
		document.menuType = "iconslabels";
	return document.menuType;
}

function changeMenuPos (newValue) {
	document.menuPos = newValue;
	setCookie("menu_pos", newValue);
	changeBodyClass();
}

function changeMenuType (newValue) {
	document.menuType = newValue;
	setCookie("menu_type", newValue);
	changeBodyClass();
}

function initScreen() {
	changeBodyClass();
	var menuPos = getMenuPos();
	document.getElementById("radio_" + menuPos).checked = true;
	var menuType = getMenuType();
	document.getElementById("radio_" + menuType).checked = true;
	
	addHandler(document,'click',onDocumentClick,false);
	addHandler(document.getElementById('body-frame').contentWindow,'click',hideViewSelector,false);
	
	setTimeout("hideViewSelector()", 30);
}

function hideViewSelector() {
	document.getElementById("view-selector").style.visibility ="hidden";
}



function showViewSelector() {
	document.processShowSelector = true;
	var selector = document.getElementById("view-selector");
	selector.className = "visible";
	selector.style.visibility = (selector.style.visibility == "visible") ? "hidden" : "visible";
}

function onDocumentClick(e){
	if (document.processShowSelector) {
		document.processShowSelector = false;
		return;
	}
	e=e||event;
  var target=e.target||e.srcElement;
  var selector = document.getElementById("view-selector");
  if(selector){
    var parent=target;
    while(parent.parentNode&&parent!=selector)
    	parent=parent.parentNode;
    if(!parent || parent != selector)
      selector.style.visibility = "hidden";
  }
}


function refreshWrapper(url) {
	var loc = document.location.href;
	loc = loc.replace(/\?.*/, "");
	document.location.href = loc + "?url=" + encodeURI(url) ;
}

function closeMessage(messageId) {
	var closedMessagesStr = getCookie("closedMessagesIds");
	closedMessages = (closedMessagesStr == null) ? [] : closedMessagesStr.split(",");
	if(!closedMessagesStr || closedMessagesStr.indexOf(messageId) == -1) {
		closedMessages.push(messageId);
		setCookie("closedMessagesIds", closedMessages.join(","));
	}	
	if (document.getElementById(messageId)) {
		document.getElementById(messageId).style.display = "none";
		showAppMessages(null);
	}
}