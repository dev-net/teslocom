<?php
/**
 * @param int $productID
 * @return bool
 */
function prdProductExists( $productID ){

  return db_phquery_fetch(DBRFETCH_FIRST, 'SELECT COUNT(*) FROM ?#PRODUCTS_TABLE WHERE productID=?', $productID);
}

function GetProduct( $productID, $is_admin = 0, $prtnr = 0){

  $productID = (int)$productID;
  $q = db_query('SELECT * FROM '.PRODUCTS_TABLE.' WHERE productID='.$productID);

  
  if ( $product=db_fetch_assoc($q) ){

    // Generate new price if partner
    if (isset($_SESSION['is_partner']) && !$is_admin){
      if (empty($prtnr)) {
        if (isset($_SESSION['choose_p'])){
          $prtnr = PRTNRgetFromId(intval($_SESSION['choose_p']));
        }
        else {
          $prtnr = PRTNRget();
        }
        $prtnr =  array('is_manager' => $prtnr['is_manager'], 'w_price' => $prtnr['w_price'], 'customerID' => $prtnr['customerID']);
      }

      $product = _PRTNRgeneratePrice($product, $prtnr, $is_admin);
    }

    LanguagesManager::ml_fillFields(PRODUCTS_TABLE, $product);
    $product["ProductIsProgram"] = 	(trim($product["eproduct_filename"]) != "");
    $sql = '
      SELECT pot.*,povt.* FROM '.PRODUCT_OPTIONS_VALUES_TABLE.' as povt
      LEFT JOIN '.PRODUCT_OPTIONS_TABLE.' as pot ON pot.optionID=povt.optionID
      WHERE productID='.$productID.'
    ';
    $Result = db_query($sql);
    $product['option_values'] = array();

    while ($_Row = db_fetch_assoc($Result)) {

      LanguagesManager::ml_fillFields(PRODUCT_OPTIONS_TABLE, $_Row);
      LanguagesManager::ml_fillFields(PRODUCT_OPTIONS_VALUES_TABLE, $_Row);
      $_Row['value'] = $_Row['option_value'];
      $product['option_values'][] = $_Row;
    }

    if (class_exists('Time')) {
      $product['date_added']=Time::standartTime( $product['date_added'] );
      $product['date_modified']=Time::standartTime( $product['date_modified'] );
    }

    $product['in_cart'] = 0;
    $in_carts = inCart();
    if (!empty($in_carts)){
      if (in_array(intval($product['productID']), $in_carts)) {
        $product['in_cart'] = 1;
      }
    }
    return $product;
  }
  return false;
}


function inCart(){
  $in_carts = array();
  $login = $_SESSION["log"];

  if (!empty($login)){
    $q = db_query("SELECT * FROM ".CUSTOMERS_TABLE." WHERE Login='{$login}'");
    $c = db_fetch_row($q);

    $q = db_query("SELECT t2.productID FROM `SC_shopping_carts` t1
        LEFT JOIN `SC_shopping_cart_items` t2 ON t1.itemID=t2.itemID
      WHERE customerID={$c['customerID']}");
    while ( $item = db_fetch_assoc($q) ){
      $pid = intval($item['productID']);
      if (!empty($pid))
        $in_carts[] = intval($item['productID']);
    }
  } else {
    if ( isset($_SESSION["gids"]) ){
      return $_SESSION["gids"];
    }
  }
  return $in_carts;
}

/**
 * Get product from code
 * @param $code
 * @return array|bool
 */
function getProductFromCode($code) {
  $q = db_query("SELECT * FROM ".PRODUCTS_TABLE." WHERE product_code LIKE '{$code}'");
  if ( $product = db_fetch_assoc($q) ){
    return $product;
  }
  return false;
}

//Purpose sets product file
function SetProductFile( $productID, $eproduct_filename ){
  db_phquery('UPDATE ?#PRODUCTS_TABLE SET eproduct_filename=? WHERE productID=?', $eproduct_filename, $productID );
}

/**
 * Create empty product and return productID
 *
 * @return int
 */
function prdCreateEmptyProduct(){

  $Register = &Register::getInstance();
  $DBHandler = &$Register->get(VAR_DBHANDLER);
  /* @var $DBHandler DataBase */

  $val = 'no name';
  $dbq_name = LanguagesManager::sql_prepareFieldInsert('name', $val);
  $dbq = '
    INSERT ?#PRODUCTS_TABLE (categoryID, '.$dbq_name['fields'].', date_added) VALUES(1, '.$dbq_name['values'].', NOW())
  ';

  $DBRes = $DBHandler->ph_query($dbq);
  if(SystemSettings::is_hosted()&&file_exists(WBS_DIR.'/kernel/classes/class.metric.php')){
    include_once(WBS_DIR.'/kernel/classes/class.metric.php');

    $DB_KEY=SystemSettings::get('DB_KEY');
    $U_ID = sc_getSessionData('U_ID');

    $metric = metric::getInstance();
    $metric->addAction($DB_KEY, $U_ID, 'SC','ADDPRODUCT','ACCOUNT', '');
  }
  return $DBRes->getInsertID();
}

// Purpose	deletes product
// Inputs   $productID - product ID
// Returns	true if success, else false otherwise
function DeleteProduct($productID, $updateGCV = 1){

  $productID = intval($productID);
  $whereClause = ' where productID='.$productID;
  //removing images from filesystem
  $q=db_query("select photoID from ".PRODUCT_PICTURES.$whereClause );

  while ( $picture=db_fetch_row($q) ){
    DeleteThreePictures( $picture['photoID'] );
  }

  $q = db_query( "select itemID from ".SHOPPING_CART_ITEMS_TABLE." ".$whereClause );
  while( $row=db_fetch_row($q) )
  db_query( "delete from ".SHOPPING_CARTS_TABLE." where itemID=".$row["itemID"] );

  // delete all items for this product
  db_query("update ".SHOPPING_CART_ITEMS_TABLE." set productID=NULL ".$whereClause);

  // delete all product option values
  db_query("delete from ".PRODUCTS_OPTIONS_SET_TABLE.$whereClause);
  db_query("delete from ".PRODUCT_OPTIONS_VALUES_TABLE.$whereClause);

  // delete pictures
  db_query("delete from ".PRODUCT_PICTURES.$whereClause);

  // delete additional categories records
  db_query("delete from ".CATEGORIY_PRODUCT_TABLE.$whereClause);

  // delete discussions
  db_query("delete from ".DISCUSSIONS_TABLE.$whereClause);

  // delete related items
  db_query("delete from ".RELATED_PRODUCTS_TABLE.$whereClause );
  db_query("delete from ".RELATED_PRODUCTS_TABLE." where Owner=$productID");

  //removing files from filesystem
  $q=db_query("select eproduct_filename as filename from ".PRODUCTS_TABLE.$whereClause);
  if ( $file=db_fetch_row($q) ){
    if($file["filename"]!=null && strlen($file["filename"])>0){
      if ( file_exists(DIR_PRODUCTS_FILES."/".$file["filename"]) ){
        Functions::exec('file_remove', array(DIR_PRODUCTS_FILES."/".$file["filename"]));
      }
    }

  }

  // delete product
  db_query("delete from ".PRODUCTS_TABLE.$whereClause);

  TagManager::removeObjectTags('product', $productID);
  ProductList::stc_deleteProductFromLists($productID);

  if ( $updateGCV == 1 && CONF_UPDATE_GCV == '1')
  update_products_Count_Value_For_Categories(1);

  return true;
}

// Purpose	gets extra parametrs
// Inputs   $productID - product ID
// Returns	array of value extraparametrs
//				each item of this array has next struture
//					first type "option_type" = 0
//						"name"					- parametr name
//						"option_value"			- value
//						"option_type"			- 0
//					second type "option_type" = 1
//						"name"					- parametr name
//						"option_show_times"		- how times does show in client side this
//												parametr to select
//						"variantID_default"		- variant ID by default
//						"values_to_select"		- array of variant value to select
//							each item of "values_to_select" array has next structure
//								"variantID"			- variant ID
//								"price_surplus"		- to added to price
//								"option_value"		- value
function GetExtraParametrs( $productID ){

  static $ProductsExtras = array();
  if(!is_array($productID)){

    $ProductIDs = array($productID);
    $IsProducts = false;
  }elseif(count($productID)) {

    $ProductIDs = &$productID;
    $IsProducts = true;
  }else {

    return array();
  }
  $ProductIDsCached = array_keys($ProductsExtras);
  $ProductIDs = array_diff($ProductIDs,$ProductIDsCached);
  if(count($ProductIDs)){
    $sql = '
      SELECT pot.*, '.LanguagesManager::sql_constractSortField(PRODUCT_OPTIONS_TABLE, 'pot.name').', povt.*
      FROM ?#PRODUCT_OPTIONS_VALUES_TABLE as povt LEFT JOIN  ?#PRODUCT_OPTIONS_TABLE as pot ON pot.optionID=povt.optionID
      WHERE povt.productID IN (?@) ORDER BY pot.sort_order, '.LanguagesManager::sql_getSortField(PRODUCT_OPTIONS_TABLE, 'pot.name').'
    ';
    $Result = db_phquery($sql, $ProductIDs);

    while ($_Row = db_fetch_assoc($Result)) {

      LanguagesManager::ml_fillFields(PRODUCT_OPTIONS_VALUES_TABLE, $_Row);
      LanguagesManager::ml_fillFields(PRODUCT_OPTIONS_TABLE, $_Row);
      $b=null;
      if (($_Row['option_type']==0 || $_Row['option_type']==NULL) && !LanguagesManager::ml_isEmpty('option_value', $_Row['option_value'])){

        $ProductsExtras[$_Row['productID']][] = $_Row;
      }
      else if ( $_Row['option_type']==1 ){

        //fetch all option values variants
        $sql = '
          SELECT povvt.*, '.LanguagesManager::sql_constractSortField(PRODUCTS_OPTIONS_VALUES_VARIANTS_TABLE, 'povvt.option_value').', post.price_surplus
          FROM '.PRODUCTS_OPTIONS_SET_TABLE.' as post
          LEFT JOIN '.PRODUCTS_OPTIONS_VALUES_VARIANTS_TABLE.' as povvt
          ON povvt.variantID=post.variantID
          WHERE povvt.optionID='.$_Row['optionID'].' AND post.productID='.$_Row['productID'].' AND povvt.optionID='.$_Row['optionID'].'
          ORDER BY povvt.sort_order, '.LanguagesManager::sql_getSortField(PRODUCTS_OPTIONS_VALUES_VARIANTS_TABLE, 'povvt.option_value').'
        ';

        $q2=db_query($sql);
        $_Row['values_to_select']=array();
        $i=0;
        while( $_Rowue = db_fetch_assoc($q2)  ){

          LanguagesManager::ml_fillFields(PRODUCTS_OPTIONS_VALUES_VARIANTS_TABLE, $_Rowue);
          $_Row['values_to_select'][$i]=array();
          $_Row['values_to_select'][$i]['option_value'] = xHtmlSpecialChars($_Rowue['option_value']);
          if ( $_Rowue['price_surplus'] > 0 )$_Row['values_to_select'][$i]['option_value'] = $_Row['values_to_select'][$i]['option_value'].' (+ '.show_price($_Rowue['price_surplus']).')';
          elseif($_Rowue['price_surplus'] < 0 )$_Row['values_to_select'][$i]['option_value'] = $_Row['values_to_select'][$i]['option_value'].' (- '.show_price(-$_Rowue['price_surplus']).')';

          $_Row['values_to_select'][$i]['option_valueWithOutPrice'] = $_Rowue['option_value'];
          $_Row['values_to_select'][$i]['price_surplus'] = show_priceWithOutUnit($_Rowue['price_surplus']);
          $_Row['values_to_select'][$i]['variantID']=$_Rowue['variantID'];
          $i++;
        }
        $ProductsExtras[$_Row['productID']][] = $_Row;
      }
    }
  }
  if(!$IsProducts){

    if(!count($ProductsExtras))return array();
    else {
      return $ProductsExtras[$productID];
    }
  }

  return $ProductsExtras;
}


function _setPictures( & $product){

  if ( isset($product['default_picture'])&&!is_null($product['default_picture'])&&isset($product['productID']) ){

    $Result = db_phquery('SELECT filename, thumbnail, enlarged FROM ?#PRODUCT_PICTURES WHERE photoID=?',$product['default_picture']);
    $Picture = db_fetch_assoc($Result);
    $product['picture'] = file_exists(DIR_PRODUCTS_PICTURES.'/'.$Picture['filename'])?$Picture['filename']:0;
    $product['thumbnail']=file_exists(DIR_PRODUCTS_PICTURES.'/'.$Picture['thumbnail'])?$Picture['thumbnail']:0;
    $product['big_picture']=file_exists(DIR_PRODUCTS_PICTURES.'/'.$Picture['enlarged'])?$Picture['enlarged']:0;
  }elseif (is_array($product)){

    $Products = &$product;
    $DefaultPictures = array();
    $TC = count($Products);
    for ($j=0;$j<$TC;$j++){
      if(!is_null($Products[$j]['default_picture']))$DefaultPictures[intval($Products[$j]['default_picture'])][] = $j;
    }
    if(!count($DefaultPictures))return;
    $sql = '
      SELECT photoID,filename, thumbnail, enlarged FROM '.PRODUCT_PICTURES.' WHERE photoID IN('.implode(',',array_keys($DefaultPictures)).')
    ';
    $Result = db_query($sql);
    while ($Picture = db_fetch_assoc($Result)){

      foreach ($DefaultPictures[$Picture['photoID']] as $j){

        $Products[$j]['picture'] = file_exists(DIR_PRODUCTS_PICTURES.'/'.$Picture['filename'])?$Picture['filename']:0;
        $Products[$j]['thumbnail']=file_exists(DIR_PRODUCTS_PICTURES.'/'.$Picture['thumbnail'])?$Picture['thumbnail']:0;
        $Products[$j]['big_picture']=file_exists(DIR_PRODUCTS_PICTURES.'/'.$Picture['enlarged'])?$Picture['enlarged']:0;
      }
    }
  }
}


function GetProductInSubCategories( $callBackParam, &$count_row, $navigatorParams = null )
{
  
  if ( $navigatorParams != null )
  {
    $offset			= $navigatorParams["offset"];
    $CountRowOnPage	= $navigatorParams["CountRowOnPage"];
  }
  else
  {
    $offset = 0;
    $CountRowOnPage = 0;
  }

  $categoryID	= $callBackParam["categoryID"];
  $subCategoryIDArray = catGetSubCategories( $categoryID );
  $cond = "";
  foreach( $subCategoryIDArray as $subCategoryID )
  {
    if ( $cond != "" )
    $cond .= " OR categoryID=$subCategoryID";
    else
    $cond .= " categoryID=$subCategoryID ";
  }
  $whereClause = "";
  if ( $cond != "" )
  $whereClause = " where ".$cond;

  $result = array();
  if ( $whereClause == "" )
  {
    $count_row = 0;
    return $result;
  }

  $langManager = &LanguagesManager::getInstance();

  $q=db_query("
  SELECT * FROM ".PRODUCTS_TABLE.
  " ".$whereClause." ORDER BY sort_order, name " );
  $i=0;

  $prtnr = 0;
  if (isset($_SESSION['is_partner'])){
    if (isset($_SESSION['choose_p'])){
      $prtnr = PRTNRgetFromId(intval($_SESSION['choose_p']));
    }
    else {
      $prtnr = PRTNRget();
    }
    $prtnr =  array('is_manager' => $prtnr['is_manager'], 'w_price' => $prtnr['w_price'], 'customerID' => $prtnr['customerID']);
  }
  
  while( $row=db_fetch_row($q) ){

    // Generate new price if partner
    if (!empty($prtnr)){
      $row = _PRTNRgeneratePrice($row, $prtnr);
    }

    LanguagesManager::ml_fillFields(PRODUCTS_TABLE, $row);
    if ( ($i >= $offset && $i < $offset + $CountRowOnPage) ||
    $navigatorParams == null  )
    {
      $row["PriceWithUnit"]		= show_price($row["Price"]);
      $row["list_priceWithUnit"] 	= show_price($row["list_price"]);
      // you save (value)
      $row["SavePrice"]		= show_price($row["list_price"]-$row["Price"]);

      // you save (%)
      if ($row["list_price"])
      $row["SavePricePercent"] = ceil(((($row["list_price"]-$row["Price"])/$row["list_price"])*100));

      _setPictures( $row );

      $row["product_extra"]=GetExtraParametrs($row["productID"]);
      $row["PriceWithOutUnit"]= show_priceWithOutUnit( $row["Price"] );

      $row['in_cart'] = 0;
      $in_carts = inCart();
      if (!empty($in_carts))
        if (in_array($row['productID'], $in_carts))
          $row['in_cart'] = 1;

      $result[] = $row;
    }
    $i++;
  }
  $count_row = $i;
  return $result;
}


// *****************************************************************************
// Purpose	gets all products by categoryID
// Inputs     	$callBackParam item
//			"categoryID"
//			"fullFlag"
// Remarks
// Returns
function prdGetProductByCategory( $callBackParam, &$count_row, $navigatorParams = null )
{

  if ( $navigatorParams != null )
  {
    $offset			= $navigatorParams["offset"];
    $CountRowOnPage	= $navigatorParams["CountRowOnPage"];
  }
  else
  {
    $offset = 0;
    $CountRowOnPage = 0;
  }

  $result = array();

  $categoryID	= $callBackParam["categoryID"];
  $fullFlag	= $callBackParam["fullFlag"];
  if ( $fullFlag )
  {
    $conditions = array( " categoryID=$categoryID " );
    $q = db_query("select productID from ".
    CATEGORIY_PRODUCT_TABLE." where  categoryID=$categoryID");
    while( $products = db_fetch_row( $q ) )
    $conditions[] = " productID=".$products[0];

    $data = array();
    $langManager = &LanguagesManager::getInstance();

    $prtnr = 0;
    if (isset($_SESSION['is_partner'])){
      if (isset($_SESSION['choose_p'])){
        $prtnr = PRTNRgetFromId(intval($_SESSION['choose_p']));
      }
      else {
        $prtnr = PRTNRget();
      }
      $prtnr =  array('is_manager' => $prtnr['is_manager'], 'w_price' => $prtnr['w_price'], 'customerID' => $prtnr['customerID']);
    }

    foreach( $conditions as $cond )
    {
   
      $q=db_query("select * from ".PRODUCTS_TABLE.
      " where ".$cond );
      while( $row = db_fetch_row($q) )
      {

        // Generate new price if partner
        if (!empty($prtnr)){
          $row = _PRTNRgeneratePrice($row, $prtnr);
        }

        LanguagesManager::ml_fillFields(PRODUCTS_TABLE, $row);
        $row["PriceWithUnit"]		= show_price($row["Price"]);
        $row["list_priceWithUnit"] 	= show_price($row["list_price"]);
        // you save (value)
        $row["SavePrice"]		= show_price($row["list_price"]-$row["Price"]);

        // you save (%)
        if ($row["list_price"])
        $row["SavePricePercent"] = ceil(((($row["list_price"]-$row["Price"])/$row["list_price"])*100));
        _setPictures( $row );
        $row["product_extra"]=GetExtraParametrs($row["productID"]);
        $row["PriceWithOutUnit"]= show_priceWithOutUnit( $row["Price"] );

        $row['in_cart'] = 0;
        $in_carts = inCart();
        if (!empty($in_carts))
          if (in_array($row['productID'], $in_carts))
            $row['in_cart'] = 1;

        $data[] = $row;
      }
    }

    function _compare( $row1, $row2 )
    {
      if ( (int)$row1["sort_order"] == (int)$row2["sort_order"] )
      return 0;
      return ((int)$row1["sort_order"] < (int)$row2["sort_order"]) ? -1 : 1;
    }

    usort($data, "_compare");

    $result = array();
    $i = 0;
    foreach( $data as $res )
    {
      if ( ($i >= $offset && $i < $offset + $CountRowOnPage) ||
      $navigatorParams == null )
      $result[] = $res;
      $i++;
    }
    $count_row = $i;
    return $result;
  }
  else
  {
    $q=db_phquery("SELECT *,".LanguagesManager::sql_constractSortField(PRODUCTS_TABLE, 'name')." FROM ?#PRODUCTS_TABLE WHERE categoryID=? order by sort_order, ".LanguagesManager::sql_getSortField(PRODUCTS_TABLE, 'name'), $categoryID );
    $i=0;
    while( $row=db_fetch_assoc($q) )
    {
      if ( ($i >= $offset && $i < $offset + $CountRowOnPage) ||
      $navigatorParams == null  )
      LanguagesManager::ml_fillFields(PRODUCTS_TABLE, $row);
      $result[] = $row;
      $i++;
    }
    $count_row = $i;
    return $result;
  }
}

/**
 * Fetch products from current category
 * 
 * @param string $condition
 * @param int $categoryID
 * @return string
 */
function _getConditionWithCategoryConj( $condition, $categoryID, $check_subcategories = false ){

  $categoryID_Array = $check_subcategories?catGetSubCategories( $categoryID ):array();
  $categoryID_Array[] = (int)$categoryID;

  $sql_category_part = 'categoryID IN ('.implode(',', xEscapeSQLstring($categoryID_Array)).')';

  $dbq = '
    SELECT productID FROM ?#CATEGORIY_PRODUCT_TABLE WHERE categoryID IN (?@)
  ';
  $dbres = db_phquery($dbq, $categoryID_Array);
  $rel_products = array();
  while ($row = db_fetch_row($dbres)){
    $rel_products[] = $row[0];
  }

  $sql_product_part = count($rel_products)?'p.productID IN ('.implode(',', xEscapeSQLstring($rel_products)).')':'';

  return ($sql_product_part?'( ('.$sql_product_part.') OR ('.$sql_category_part.') )':'('.$sql_category_part.')').($condition?' AND ('.$condition.')':'');
}

/**
 * @param $template
 * @return array
 */
function _prepareSearchExtraParameters($template){

  $sqls_joins = array();
  $sqls_options = array();
  $categoryID = $template['categoryID'];
  $sqls_params = array();

  $cnt = 0;
  $_count = 0;
  foreach( $template as $key => $item ){

    if(!isset($item['optionID']))continue;
    if($item['value'] === '')continue;
    if($key === 'categoryID' )continue;

    // get value to search
    $res = schOptionIsSetToSearch( $categoryID, $item['optionID'] );

    if($res['set_arbitrarily'] && $item['value'] === '0')continue;

    if($res['set_arbitrarily']){
      $sqls_joins[] = '
        LEFT JOIN ?#PRODUCT_OPTIONS_VALUES_TABLE PrdOptVal'.$cnt.' ON p.productID=PrdOptVal'.$cnt.'.productID
        LEFT JOIN ?#PRODUCTS_OPTIONS_SET_TABLE PrdOptSet'.$cnt.' ON p.productID=PrdOptSet'.$cnt.'.productID
        LEFT JOIN ?#PRODUCTS_OPTIONS_VALUES_VARIANTS_TABLE PrdOptValVar'.$cnt.' ON
          PrdOptSet'.$cnt.'.optionID=PrdOptValVar'.$cnt.'.optionID AND
          PrdOptSet'.$cnt.'.variantID=PrdOptValVar'.$cnt.'.variantID
      ';
      $search_name = 'option_value_'.($_count++);
      $sqls_params[$search_name] = '%'._searchPatternReplace($item['value']).'%';
      $sqls_options[] = '
        PrdOptVal'.$cnt.'.optionID='.intval($item['optionID']).'
        AND
        (
        ( PrdOptVal'.$cnt.'.option_type=1
        AND PrdOptValVar'.$cnt.'.'.LanguagesManager::sql_prepareField('option_value').' LIKE ?'.$search_name.
        ') OR (
        PrdOptVal'.$cnt.'.option_type=0
        AND PrdOptVal'.$cnt.'.'.LanguagesManager::sql_prepareField('option_value').' LIKE ?'.$search_name.
        ')
        )
      ';

    }else{

      $sqls_joins[] = '
        LEFT JOIN ?#PRODUCT_OPTIONS_VALUES_TABLE PrdOptVal'.$cnt.' ON p.productID=PrdOptVal'.$cnt.'.`productID`
        LEFT JOIN ?#PRODUCTS_OPTIONS_SET_TABLE PrdOptSet'.$cnt.' ON p.productID=PrdOptSet'.$cnt.'.`productID`
      ';
    /*	$sqls_options[] = '
        PrdOptVal'.$cnt.'.optionID='.intval($item['optionID']).'
        AND PrdOptVal'.$cnt.'.option_type=1
        AND PrdOptSet'.$cnt.'.variantID='.intval($item['value']).'
      ';
      */

      /* !!!!!!!!!!!!!! */

      // get real option text value - required to search for fixed option values

      $qtmp = db_phquery( "select ".LanguagesManager::sql_prepareField('option_value')." from ?#PRODUCTS_OPTIONS_VALUES_VARIANTS_TABLE".
          " where optionID=? and variantID=?",intval($item['optionID']),intval($item['value']));

      $rowtmp = db_fetch_row($qtmp);

      if ($rowtmp)
        $item_text_value = $rowtmp[0];
      else
        $item_text_value = "";

      //changed this fragment to support search in fixed product parameters.
      $search_name = 'option_value_'.($_count++);
      $sqls_params[$search_name] = '%'.$item_text_value.'%';
      $sqls_options[] = '
        PrdOptVal'.$cnt.'.optionID='.intval($item['optionID']).'
        AND
        (
          ( PrdOptVal'.$cnt.'.option_type=1 AND PrdOptSet'.$cnt.'.variantID='.intval($item['value']).')
           OR
          (PrdOptVal'.$cnt.'.option_type=0 AND PrdOptVal'.$cnt.'.'.LanguagesManager::ml_getLangFieldName('option_value').' LIKE ?'.$search_name.')
        )';

/* !!!!!!!!!!!!!! */
    }
    $cnt++;
  }

  return array('where' => $sqls_options, 'join' => $sqls_joins, 'params'=>$sqls_params);
}

function _searchPatternReplace($string){
  static $patterns = array('/\\\\/',
              '/%/',
              '/_/',
              '/(^|[^\/]{1})(\?)/',
              '/([\/]{1})(\?)/',
              '/(^|[^\/]{1})(\*)/',
              '/([\/]{1})(\*)/',
              '/(^|[^\/]{1})\+/',
              '/([\/]{1})\+/',
              );
  static $replacements = array('\\\\\\\\',
                '\\%',
                '\\_',
                '\\1_',
                '?',
                '\\1%',
                '*',
                '\\1 ',
                '+');
  return preg_replace($patterns,$replacements,$string);
  //return $res;
  //return str_replace(array('%','_','?','%'),array('\\%','\\_','_','%'),$string);
}

/**
 * @param $callBackParam
 * @param $count_row
 * @param null $navigatorParams
 * @return array
 */
function prdSearchProductByTemplate($callBackParam, &$count_row, $navigatorParams = null){

  $limit = $navigatorParams != null?' LIMIT '.(int)$navigatorParams['offset'].','.(int)$navigatorParams['CountRowOnPage']:'';
  $where = '';
  $_sqlParams = array();
  
  $where_arr = array();

  $add_setting = prdSubSetting();
  if ( isset($callBackParam['search_simple']) ){

    $where_arr['search'] = '';
    if (!empty($callBackParam['search_simple'])) {
      $term = implode(' ', $callBackParam['search_simple']);
      $where_name = array();
      $where_description = array();
      $where_brief_description = array();
      $where_sub = array();
      foreach ($callBackParam['search_simple'] as $t) {
        $t = mb_strtolower($t,'UTF-8');
        $where_name[] = "LOWER(".LanguagesManager::sql_prepareField('name').") LIKE '%{$t}%'";
        $where_description[] = "LOWER(".LanguagesManager::sql_prepareField('description').") LIKE '%{$t}%'";
        $where_brief_description[] = "LOWER(".LanguagesManager::sql_prepareField('brief_description').") LIKE '%{$t}%'";
        if ($add_setting) {
          $where_sub[] = "LOWER(ps.name) LIKE '%{$t}%'";
        }
      }

      $where_arr['search'] = "((".implode(" AND ", $where_name).") OR
                               (".implode(" AND ", $where_description).") OR
                               (".implode(" AND ", $where_brief_description).") OR
                               (LOWER(product_code) LIKE '%{$term}%'))";

      if ($add_setting) {
        $where_sub = 'AND (('.implode(' AND ', $where_sub).') OR ( CONCAT(p.product_code," (",ps.sid,")") LIKE "%'.$term.'%") )';
      }
    }

    // Condition search brand
    $where_arr['searchbrand_str'] = array();
    if (isset($callBackParam['searchbrand'])) {
      if (!empty($callBackParam['searchbrand'])) {
        $where_brief_description = array();
        $where_sub = array();
        $terms = explode(' ', trim(strip_tags($callBackParam['searchbrand'])));
        foreach ($terms as $t) {
          $t = mb_strtolower($t,'UTF-8');
          $where_brief_description[] = "LOWER(".LanguagesManager::sql_prepareField('brief_description').") LIKE '%{$t}%'";
          if ($add_setting) {
            $where_sub[] = "LOWER(ps.name) LIKE '%{$t}%'";
          }
        }
        $where_arr['searchbrand_str'] = "(".implode(" AND ", $where_brief_description).")";

        if ($add_setting) {
          $where_sub = 'AND ('.implode(' AND ', $where_sub).')';
        }
      }
    }

    // Condition categories of brand
    $where_arr['bid_str'] = array();
    if (isset($callBackParam['bid'])) {
      if (!empty($callBackParam['bid'])) {
        $brand = prdGetBrandFromId(intval($callBackParam['bid']));
        if (!empty($brand)) {
          $where_arr['bid_str'] = "(categoryID IN ({$brand['cids']}))";

          if ($add_setting) {
            $where_sub .= " AND (p.categoryID IN ({$brand['cids']}))";
          }
        }
      }
    }
  }
  else{

    $where_arr['enabled'] = '';
    if (isset($callBackParam['enabled'])){
      $where_arr['enabled'] = "enabled = '".xEscapeSQLstring($callBackParam['enabled'])."'";
    }

    $where_arr['name'] = '';
    if ( isset($callBackParam['name']) ){
      $_count = 0;
      $where_name = array();
      foreach( $callBackParam['name'] as $t ){
        $t = mb_strtolower($t,'UTF-8');
        $where_name[] = "(LOWER(".LanguagesManager::sql_prepareField('name').") LIKE '%{$t}%')";
      }
      $where_arr['search'] = "(".implode(" AND ", $where_name).")";
    }

    $where_arr['product_code'] = '';
    if(isset($callBackParam['product_code'])){
      $term = implode(' ', $callBackParam['product_code']);
      $where_arr['product_code'] = "product_code LIKE '%{$term}%'";
    }

    $where_arr['from'] = '';
    if(isset($callBackParam['price']['from']))
      $where_arr['from'] = ConvertPriceToUniversalUnit($callBackParam['price']['from']).'<=Price';
    
    $where_arr['to'] = '';
    if(isset($callBackParam['price']['to']))
      $where_arr['to'] = 'Price<='.ConvertPriceToUniversalUnit($callBackParam['price']['to']);
    
    $where_arr['!productID'] = '';
    if(isset($callBackParam['!productID']))
      $where_arr['!productID'] = 'productID<>'.xEscapeSQLstring($callBackParam['!productID']);
    
    if ( isset($callBackParam['categoryID'])){
      $where_arr_tmp = array();
      foreach ($where_arr as $where_arr_el){
        if (!empty($where_arr_el)) $where_arr_tmp[] = $where_arr_el;
      }
            
      if (!empty($where_arr_tmp))
        $where = implode(' AND ', $where_arr_tmp);

      $where = 'WHERE '._getConditionWithCategoryConj( $where, $callBackParam['categoryID'], isset($callBackParam['searchInSubcategories'])&&$callBackParam['searchInSubcategories']);
      $where_arr = array();
    }
  }
    
  $sort_field = 'name';
  $order_by = ' ORDER BY sort_order, '.LanguagesManager::sql_getSortField(PRODUCTS_TABLE, $sort_field);

  if (isset($callBackParam['sort'])&&in_array($callBackParam['sort'],array('name','brief_description','in_stock','Price','customer_votes','customers_rating','list_price','sort_order','items_sold','product_code','shipping_freight'))){
    $order_by = ' ORDER BY '.LanguagesManager::sql_getSortField(PRODUCTS_TABLE, $callBackParam['sort']).' ASC ';
    if (isset($callBackParam['direction'])&&$callBackParam['direction'] == 'DESC')
      $order_by = ' ORDER BY '.LanguagesManager::sql_getSortField(PRODUCTS_TABLE, $callBackParam['sort']).' DESC ';
  }

  /**
   * Seach by extra parameters
   */
  $where_arr['extra'] = '';
  if(isset($callBackParam['extraParametrsTemplate'])){
    $_sqls = _prepareSearchExtraParameters($callBackParam['extraParametrsTemplate']);
    if(count($_sqls['where'])){
      $left_join = implode(' ', $_sqls['join']);
      $where_arr['extra'] = '('.implode(') AND (',$_sqls['where']).')';
      $group_by = ' GROUP BY p.productID';
    }
    $_sqlParams = array_merge($_sqlParams, $_sqls['params']);
  }

  if (empty($where)) {
    if (isset($callBackParam['isAdmin'])) {
      $where = "WHERE 1";
    } else {
      $where = "WHERE categoryID<>1 AND enabled=1";
    }
  }

  $where_arr_tmp = array();
  foreach ($where_arr as $where_arr_el){
    if (!empty($where_arr_el)) $where_arr_tmp[] = $where_arr_el;
  }

  if (isset($_SESSION['pt'])){
    if ($_SESSION['pt'] == 2) {
      $where .= " AND product_code LIKE '__-__-____'";
    }
  }

  if ( (isset($where_arr['name']) && !empty($where_arr['name']) ) ||
    (isset($where_arr['product_code']) && !empty($where_arr['product_code']) ))
    $where .= ' AND ('.implode(' OR ', $where_arr_tmp).')';
  elseif (!empty($where_arr_tmp))
    $where .= ' AND '.implode(' AND ', $where_arr_tmp);

  $dbq = 'SELECT COUNT(DISTINCT p.productID) as cnt FROM '.PRODUCTS_TABLE.' p '.$left_join.$where;
  $count_row = db_phquery_fetch(DBRFETCH_FIRST, $dbq, $_sqlParams);

  // Sub count of
  if ($add_setting && $where_sub) {
    $dbq = 'SELECT COUNT(DISTINCT ps.sid) as cnt
      FROM '.PRODUCTS_TABLE.' AS p
        JOIN '.PRODUCTS_ITEMS.' AS pi ON pi.pid = p.productID
        JOIN '.PRODUCTS_SUB.' AS ps ON ps.iid = pi.iid
      WHERE p.categoryID<>1 AND p.enabled = 1 '.$where_sub;
    $count_row += db_phquery_fetch(DBRFETCH_FIRST, $dbq, $_sqlParams);
  }

  if(isset($navigatorParams['offset'])&&$count_row<$navigatorParams['offset']){
    $navigatorParams['offset'] = $navigatorParams['CountRowOnPage']*intval($count_row/$navigatorParams['CountRowOnPage']);
  }

  $limit = $navigatorParams != null?' LIMIT '.(int)$navigatorParams['offset'].','.(int)$navigatorParams['CountRowOnPage']:'';

  $dbq = 'SELECT p.*, p.name_ru AS product_name, p.product_code AS _product_code FROM '.PRODUCTS_TABLE.' p '.$left_join.$where.$group_by.' '.$limit;

  if ($add_setting && $where_sub) {
    $dbq = '
      SELECT p.*, p.name_ru AS product_name, NULL AS noindex, NULL AS slug_sub, NULL AS sid, p.product_code AS _product_code
        FROM '.PRODUCTS_TABLE.' AS p
      '.$where.'
      UNION ALL
      SELECT p.*, ps.name AS product_name, ps.noindex, pi.slug as slug_sub, ps.sid, CONCAT(p.product_code," (",ps.sid,")") AS _product_code
        FROM '.PRODUCTS_TABLE.' AS p
        JOIN '.PRODUCTS_ITEMS.' AS pi ON pi.pid = p.productID
        JOIN '.PRODUCTS_SUB.' AS ps ON ps.iid = pi.iid
      WHERE p.categoryID<>1 AND p.enabled = 1 '.$where_sub.' ORDER BY product_name ASC '.$limit;
  }

  $Result = db_phquery($dbq, $_sqlParams);

  $Products = array();
  $ProductsIDs = array();
  $Counter = 0;

  $prtnr = 0;
  if (isset($_SESSION['is_partner'])){
    if (isset($_SESSION['choose_p'])){
      $prtnr = PRTNRgetFromId(intval($_SESSION['choose_p']));
    }
    else {
      $prtnr = PRTNRget();
    }
    $prtnr =  array('is_manager' => $prtnr['is_manager'], 'w_price' => $prtnr['w_price'], 'customerID' => $prtnr['customerID']);
  }

  while ($_Product = db_fetch_assoc($Result)) {

    // Generate new price if partner
    if (!empty($prtnr)){
      $_Product = _PRTNRgeneratePrice($_Product, $prtnr, $callBackParam['isAdmin']);
    }
    
    LanguagesManager::ml_fillFields(PRODUCTS_TABLE, $_Product);
    if (!$_Product["productID"] && ($_Product[0]>0)) $_Product["productID"] = $_Product[0];
    $_Product['PriceWithUnit'] = show_price($_Product['Price']);
    
    $_Product['list_priceWithUnit'] = show_price($_Product['list_price']);
    // you save (value)
    $_Product['SavePrice'] = show_price($_Product['list_price']-$_Product['Price']);
    // you save (%)
    if($_Product['list_price'])$_Product['SavePricePercent'] = ceil(((($_Product['list_price']-$_Product['Price'])/$_Product['list_price'])*100));
    $_Product['PriceWithOutUnit']	= show_priceWithOutUnit( $_Product['Price'] );
    if ( ((float)$_Product['shipping_freight']) > 0 )
    $_Product['shipping_freightUC'] = show_price( $_Product['shipping_freight'] );
    $ProductsIDs[$_Product['productID']] = $Counter;

    $_Product['in_cart'] = 0;
    $in_carts = inCart();
    if (!empty($in_carts))
      if (in_array($_Product['productID'], $in_carts))
        $_Product['in_cart'] = 1;

    $Products[] = $_Product;
    $Counter++;
  }
    
  $ProductsExtra = GetExtraParametrs(array_keys($ProductsIDs));
  foreach ($ProductsExtra as $_ProductID=>$_Extra){

    $Products[$ProductsIDs[$_ProductID]]['product_extra'] = $_Extra;
  }
  _setPictures($Products);

  return $Products;
}
/*
function clearWord($string){
  return removePunctuation(trim(mb_strtolower(strip_tags(html_entity_decode($string)),'UTF-8')));
}

function removePunctuation($string){
  return preg_replace(array('/,/','/;/','/:/','/!/','/"/','/\'/','/\(/','/\)/','/\./','/\#/','/\&/','/\?/'), " ", $string); 
}
*/
function prdSetProductDefaultPicture($productID, $photoID){

  db_phquery("UPDATE ?#PRODUCTS_TABLE SET default_picture=? WHERE productID=?", $photoID, $productID);
}

function prdGetMetaTags( $productID ){ //gets META keywords and description - an HTML code to insert into <head> section

  $productID = (int) $productID;

  $q = db_query( "SELECT ".LanguagesManager::sql_prepareField('meta_description')." AS meta_description, ".LanguagesManager::sql_prepareField('meta_keywords')." AS meta_keywords FROM ".PRODUCTS_TABLE." WHERE productID=".$productID );

  $row = db_fetch_row($q);
  $meta_description	= trim($row["meta_description"]);
  $meta_keywords = trim($row["meta_keywords"]);

  $res = '';

  if  ( $meta_description != '' )
  $res .= "<meta name=\"description\" content=\"".xHtmlSpecialChars($meta_description)."\">\n";
  if  ( $meta_keywords != '' )
  $res .= "<meta name=\"keywords\" content=\"".xHtmlSpecialChars($meta_keywords)."\" >\n";

  return $res;
}

function haveProductSelectableOptions($product_id)
{
    $sql = "select count(optionID) as cnt from ".PRODUCT_OPTIONS_VALUES_TABLE." ".
           "where productID = {$product_id} and option_type = 1";
    $res = db_query($sql);
    $row = db_fetch_assoc($res);
    
    return ($row['cnt'] > 0);
}

/**
 * @param $term
 * @return array
 */
function ajaxFindProducts($term){
  $results = array();
  $count = 0;
  $terms = explode(' ', $term);
  $add_setting = prdSubSetting();

  if (!empty($terms)) {
    $where_name = array();
    $where_description = array();
    $where_brief_description = array();
    $where_sub = array();
    foreach ($terms as $t) {
      $t = trim(strip_tags(mb_strtolower($t,'UTF-8')));
      if (!empty($t)) {
        $where_name[] = "LOWER(name_ru) LIKE '%{$t}%'";
        $where_description[] = "LOWER(description_ru) LIKE '%{$t}%'";
        $where_brief_description[] = "LOWER(brief_description_ru) LIKE '%{$t}%'";
        if ($add_setting) {
          $where_sub[] = "LOWER(ps.name) LIKE '%{$t}%'";
        }
      }
    }

    $where = "WHERE ( (".implode(" AND ", $where_name).") OR
                      (".implode(" AND ", $where_description).") OR
                      (".implode(" AND ", $where_brief_description).") OR
                      (product_code LIKE '{$term}')
                    ) AND categoryID<>1 AND enabled = 1";

    if ($add_setting) {
      $where_sub = 'WHERE p.categoryID<>1 AND p.enabled = 1 AND (('.implode(' AND ', $where_sub).') OR ( CONCAT(p.product_code,\' (\',ps.sid,\')\') LIKE "%'.$term.'%") )';
    }

    if (isset($_SESSION['pt'])){
      if ($_SESSION['pt'] == 2) {
        $where .= " AND product_code LIKE '__-__-____'";
      }
    }

    $limit = ' LIMIT 0, 10';

    $dbq = "SELECT * FROM ".PRODUCTS_TABLE." {$where} ORDER BY name_ru ASC ".$limit;

    if ($add_setting && $where_sub) {
      $dbq = '
        SELECT p.*, p.name_ru AS product_name, NULL AS noindex, NULL AS slug_sub, NULL AS sid, p.product_code AS _product_code
          FROM '.PRODUCTS_TABLE.' AS p
        '.$where.'
        UNION ALL
        SELECT p.*, ps.name AS product_name, ps.noindex, pi.slug as slug_sub, ps.sid, CONCAT(p.product_code,\' (\',ps.sid,\')\') AS _product_code
          FROM '.PRODUCTS_TABLE.' AS p
          JOIN '.PRODUCTS_ITEMS.' AS pi ON pi.pid = p.productID
          JOIN '.PRODUCTS_SUB.' AS ps ON ps.iid = pi.iid
        '.$where_sub.' '.$limit;
    }

    $result = db_query($dbq);
    while ($r = db_fetch_assoc($result)) {
      if ($add_setting) {
        $results[] = array("id" => $r['productID'], "name" => $r['product_name'], "slug_sub" => (!empty($r['slug_sub']) ? $r['slug_sub'] : ""), "noindex" => (!empty($r['noindex']) ? 'rel="nofollow"' : ""));
      } else {
        $results[] = array("id" => $r['productID'], "name" => $r['product_name']);
      }
    }
/*
    $q = db_query("SELECT count(*) as c FROM ".PRODUCTS_TABLE." {$where}");
    $r = db_fetch_assoc($q);
    $count =  $r['c'];
*/
  }

  return array('products' => $results, 'count' => $count, 'success' => true );
}

/**
 * @param $bid
 * @param $term
 * @param int $start
 * @return array
 */
function ajaxFindModels($bid, $term, $start = 0){
  return prdModels($bid, $term, $start);
}

/**
 * @return array
 */
function prdGetBrands() {
  $brands = array();
  $q = db_query("SELECT * FROM ".CATEGORIES_TABLE." ORDER BY name_ru ASC");
  while ($r = db_fetch_assoc($q)) {
    if ($r['categoryID'] > 1) {
      if(!preg_match('/[^0-9a-zA-Z_ ]/', $r['name_ru'])) {
        $brands[strtolower($r['name_ru'])]['name'] = $r['name_ru'];
        if (!empty($brands[strtolower($r['name_ru'])]['cids']))
          $brands[strtolower($r['name_ru'])]['cids'] .= ",";
        $brands[strtolower($r['name_ru'])]['cids'] .= $r['categoryID'];
      }
    }
  }

  return $brands;
}

/**
 * @param $bid
 * @param $model
 * @return bool
 */
function prdCheckModels($bid, $model){
  $q = db_query("SELECT * FROM ".PRODUCT_MODELS." WHERE bid = '{$bid}' AND name = '{$model}'");
  $r = db_fetch_assoc($q);
  if (!empty($r)) {
    return true;
  }
  return false;
}

/**
 * @param $text
 * @param int $continue
 * @return array
 */
function prdGetItems($text, $continue = 0) {
  $__models = array();
  if (preg_match('/\(part number\)/i', $text) !== 0 || preg_match('/\(models\)/i', $text) !== 0 || $continue) {
    $series_array = prdGetSeries();
    $text = htmlspecialchars_decode(strip_tags(preg_replace('/<\/?br|&nbsp;/i', ' ', $text)));
    $text = preg_replace('/[^\w+\d+\b\-\b\b\.\b\b\"\b\b\'\b\b\/\b]/i', ' ', $text);
    $text = trim(preg_replace('/(\n+|\s+)/', ' ', $text));

    $_models = explode(' ', $text);
    $_models = array_filter($_models);
    $series = "";  $i = 0;  $k = 0;
    foreach ($_models as $_model) {
      $_model = trim(preg_replace('/\s+/', ' ',$_model));
      if (!empty($_model)) {
        $__models[$i] = (isset($__models[$i])) ? $__models[$i] .= " " : "";

        if (preg_match('/^\D+$/', $_model)) {
          if ($k) $series = '';

          $_series = trim(preg_replace('/\s+/', ' ', $series." ".$_model));
          if ($s_name = preg_grep( '/^'.$_series.'$/i' , $series_array )) {
            $series = array_shift($s_name);
          }
          $k = 0;
        } else {
          $_model = preg_replace('/(\B\-\b|\b\-\B|\B\.\b|\b\.\B|\B\/\b|\b\/\B)/', ' ', trim($series." ".$_model));
          $_model =  trim(preg_replace('/\s+/', ' ',$_model));
          if (!empty($_model) && strlen($_model) > 1) {
            $__models[$i++] = $_model;
            $k = 1;
          }
        }
      }
    }
    $__models = array_filter($__models);
  }
  return $__models;
}

/**
 * @return bool
 */
function prdClearBrands () {
  db_query("DELETE FROM ".PRODUCT_BRANDS." WHERE 1");
  db_query("ALTER TABLE ".PRODUCT_BRANDS." AUTO_INCREMENT = 1");
  prdCreateBrands();
  return true;
}

/**
 * @param $bid
 * @return bool
 */
function prdClearModels ($bid) {
  $where_bid = "WHERE 1";
  if ($bid > 0) {
    $where_bid = " WHERE bid = '{$bid}'";
  }

  db_query("DELETE FROM ".PRODUCT_MODELS." {$where_bid}");
  return true;
}

/**
 * @return bool
 */
function prdCreateBrands() {
  $brands = prdGetBrands();

  $valuesArr = array();
  foreach($brands as $key => $brand){
    $key = mysql_real_escape_string( $key );
    $brand['name'] = mysql_real_escape_string( $brand['name'] );
    $brand['cids'] = mysql_real_escape_string(  $brand['cids'] );
    $valuesArr[] = "('$key', '{$brand['name']}', '{$brand['cids']}')";
  }
  db_query("INSERT INTO ".PRODUCT_BRANDS." (`name`, `label`, `cids`) VALUES ".implode(',', $valuesArr));

  return false;
}

/**
 * @param $html
 * @return string
 */
function closeTags ( $html )
{
  #put all opened tags into an array
  preg_match_all ( "#<([a-z]+)( .*)?(?!/)>#iU", $html, $result );
  $openedtags = $result[1];
  #put all closed tags into an array
  preg_match_all ( "#</([a-z]+)>#iU", $html, $result );
  $closedtags = $result[1];
  $len_opened = count ( $openedtags );
  # all tags are closed
  if( count ( $closedtags ) == $len_opened ) {
    return $html;
  }
  $openedtags = array_reverse ( $openedtags );
  # close tags
  for( $i = 0; $i < $len_opened; $i++ )	{
    if ( !in_array ( $openedtags[$i], $closedtags ) )		{
      $html .= "</" . $openedtags[$i] . ">";
    }
    else {
      unset ( $closedtags[array_search ( $openedtags[$i], $closedtags)] );
    }
  }
  return $html;
}

/**
 * @param $text
 * @param int $pid
 * @param array $items
 * @param int $subSetting
 * @param int $return
 * @param int $return_items
 * @return array|mixed|string
 */
function prdCompatibility($text, $pid = 0, $items = array(), $subSetting = 0, $return = 0, $return_items = 0) {
  $title_part_numbers = '';  $part_numbers = '';
  $title_models = '';      $models = '';

  // preparse text
  $_text = $text;
  $text = htmlspecialchars_decode(strip_tags(preg_replace('/<\/?br|&nbsp;/i', ' ', $text)));
  $text = preg_replace('/(\s+|\n+)/i', ' ', $text);
  $text = preg_replace('/\(?part number\)?/i', '(Part number)', $text);
  $text = preg_replace('/\(?models\)?/i', '(Models)', $text);

  if (preg_match('/\(part number\)/i', $text) !== 0 || preg_match('/\(models\)/i', $text) !== 0) {
    // get part number
    if (preg_match('/\(part number\)/i', $text) !== 0) {
      $title_part_numbers =  trim(preg_replace('/(.*)(\(part number\))(.*)/i', '$1', $text));

      if (preg_match('/\(models\)/i', $text) !== 0) {
        $part_numbers = preg_replace('/(.*)(\(part number\))(.*)\(models\)(.*)/i', '$3', $text);
      } else {
        $part_numbers = preg_replace('/(.*)(\(part number\))(.*)/i', '$3', $text);
      }
    }

    // get models
    if (preg_match('/\(models\)/i', $text) !== 0) {

      if (preg_match('/\(part number\)/i', $text) !== 0) {
        $title_models = preg_replace('/(.*)(\(part number\))(.*)(\(models\))(.*)/i', '$3', $text);
        $title_models = trim(preg_replace('/\W+\w+/', '', $title_models));
        $models = preg_replace('/(.*)(\(part number\))(.*)(\(models\))(.*)/i', '$5', $text);
      } else {
        $title_models = preg_replace('/(.*)(\(models\))(.*)/i', '$1', $text);
        $title_models = trim(preg_replace('/\W+\w+/', '', $title_models));
        $models = preg_replace('/(.*)(\(models\))(.*)/i', '$3', $text);
      }
    }
  }

  if (empty($items)) {
    // get items
    if (!empty($part_numbers))
      $part_numbers = prdGetItems($part_numbers, 1);
    if (!empty($models))
      $models = prdGetItems($models, 1);
  } else {
    // get items from param
    if (isset($items['part_numbers']))
      $part_numbers = $items['part_numbers'];
    if (isset($items['models']))
      $models = $items['models'];
  }

  if ($return_items)
    return array('part_numbers' => $part_numbers, 'models' => $models);

  // generate compatibility
  if ($subSetting) {
    $sub_products = prdGetSlugs($pid);
    $params = array('pid' => $pid, 'slugs' => $sub_products['slugs'], 'noindex' => $sub_products['noindex']);

    if (count($part_numbers)) {
      $part_numbers = array_map( function ($el) use ($params) {
        $slug = array_search($el, $params['slugs']);

        if ($slug) {
          $noindex = '';
          if (isset($params['noindex'][$slug]) && !empty($params['noindex'][$slug])) {
            $noindex = 'rel="nofollow"';
          }
          return "<a href=\"/product/{$params['pid']}/{$slug}\" {$noindex} target=\'_blank\'>{$el}</a>";
        }
        else
          return $el;
      }, $part_numbers );
    }

    if (count($models)) {
      $models = array_map( function ($el) use ($params) {
        $slug = array_search($el, $params['slugs']);

        if ($slug) {
          $noindex = '';
          if (isset($params['noindex'][$slug]) && !empty($params['noindex'][$slug])) {
            $noindex = 'rel="nofollow"';
          }
          return "<a href=\"/product/{$params['pid']}/{$slug}\" {$noindex} target=\'_blank\'>{$el}</a>";
        }
        else
          return $el;
      }, $models );
    }
  }
  $part_numbers = implode(', ', $part_numbers);
  $models = implode(', ', $models);

  if (!empty($part_numbers) || !empty($models)) {
    $text = "";
    if (!empty($part_numbers)) {
      $text .= "<p><strong>{$title_part_numbers}</strong> (Part number)<br><div class='block-models'>{$part_numbers}</div></p>";
    }
    if (!empty($models)) {
      $text .= "<p><strong>{$title_models}</strong> (Models)<br><div class='block-models'>{$models}</div></p>";
    }
    $_text = $text;
  }

  if ($return)
    return $_text;

  db_phquery("UPDATE ?#PRODUCTS_TABLE SET brief_description_ru=? WHERE productID=?", $_text, $pid);
}

/**
 * @param $pid
 * @param $item
 * @return bool
 */
function prdGetSid($pid, $item) {
  $q = db_query("SELECT * FROM ".PRODUCTS_SUB." WHERE pid = '{$pid}' AND item = '{$item}'");
  $r = db_fetch_assoc($q);
  if (!empty($r)) {
    return $r['sid'];
  } else {
    return false;
  }
}

/**
 * @param $pid
 * @return array
 */
function prdGetSlugs($pid) {
  $slugs = array();
  $noindex = array();
  $q = db_query("SELECT pi.*, ps.*, psc.name AS name_custom, psc.noindex AS noindex_custom FROM ".PRODUCTS_ITEMS." AS pi
                    JOIN ".PRODUCTS_SUB." AS ps ON ps.iid=pi.iid
                    LEFT JOIN ".PRODUCTS_SUB_CUSTOM." AS psc ON psc.pid=pi.pid AND psc.item=pi.item
                    WHERE pi.pid = '{$pid}'");
  while ($r = db_fetch_assoc($q)) {
    $slugs[$r['slug']] = $r['item'];
    $noindex[$r['slug']] = (int) (isset($r['name_custom']) && !empty($r['name_custom']) ? $r['noindex_custom'] : $r['noindex']);
  }
  return array('slugs' => $slugs, 'noindex' => $noindex);
}

/**
 * @param $step
 * @param int $per_step
 * @param $bid
 * @param $re_compatibility
 * @param $re_models
 * @return bool
 */
function prdCreateModels($step, $per_step = 10, $bid, $re_compatibility = 0, $re_models = 1) {
  $where_bid = "";
  if ($bid > 0) {
    $brand = prdGetBrandFromId($bid);
    if (!empty($brand['cids'])) {
      $where_bid = "AND (categoryID in ({$brand['cids']}))";
    }
  }

  $brands    = prdBrands();
  $subModule = prdSubSetting();

  $q = db_query("SELECT * FROM ".PRODUCTS_TABLE." WHERE 1 {$where_bid} ORDER BY productID ASC LIMIT {$step}, {$per_step}");
  while ($r = db_fetch_assoc($q)) {
    $brand = prdGetBrand($brands, $r['categoryID']);
    $items = prdCompatibility($r['brief_description_ru'], 0, 0, 0, 0, 1);
    //$models =  prdGetModelsFromDescription($r['brief_description_ru']);

    if ($re_compatibility) {
      prdCompatibility($r['brief_description_ru'], $r['productID'], $items, $subModule);
    }

    $models = $items['models'];
    if (!empty($models) && $re_models != 3) {
      $valuesArr = array();
      foreach($models as $model){
        $bid = (int) $brand['id'];
        $model = trim(mysql_real_escape_string( preg_replace('/\s+/', ' ', $model) ));

        if (!empty($model) && !empty($bid)) {
          $valuesArr[] = "('$bid', '$model')";
        }
      }

      if (!empty($valuesArr)) {
        db_query("INSERT IGNORE INTO ".PRODUCT_MODELS." (`bid`, `name`) VALUES ".implode(',', $valuesArr));
      }
    }
  }
  return true;
}

/**
 * @return array
 */
function prdBrands() {
  $brands = array();
  $q = db_query("SELECT * FROM ".PRODUCT_BRANDS." ORDER BY name ASC");
  while ($r = db_fetch_assoc($q)) {
    $brands[] = array('label' => $r['label'], 'name' => $r['name'], 'id' => $r['id'], 'cids' => $r['cids']);
  }
  return $brands;
}

/**
 * @param $bid
 * @return array|bool
 */
function prdGetBrandFromId($bid) {
  $q = db_query("SELECT * FROM ".PRODUCT_BRANDS." WHERE id = '{$bid}'");
  $r = db_fetch_assoc($q);
  if (!empty($r)) {
    return $r;
  }
  return false;
}

/**
 * @param $cid
 * @return mixed
 */
function prdGetBid($cid){
  $brands = prdBrands();
  foreach ( $brands as $brand ) {
    $cids = explode(',', $brand['cids']);
    foreach ( $cids as $_cid ) {
      if ( $_cid == $cid ) {
        return $brand['id'];
      }
    }
  }
}

/**
 * @param $brands
 * @param $cid
 * @return mixed
 */
function prdGetBrand($brands, $cid) {
  foreach ( $brands as $brand ) {
    $cids = explode(',', $brand['cids']);
    foreach ( $cids as $_cid ) {
      if ( $_cid == $cid ) {
        return $brand;
      }
    }
  }
}

/**
 * @param $bid
 * @param string $search
 * @return array
 */
function prdModels($bid, $search = '', $start = 0) {
  $models = array();
  $count = 0;
  $where = "";
  if (!empty($search)) {
    $search = trim(strip_tags($search));
    if (!empty($search)) {
      $where = " AND name LIKE '%{$search}%'";
    }
  }

  $where = "";
  if (!empty($search)) {
    $where_search = array();
    $terms = explode(' ', trim(strip_tags($search)));
    foreach ($terms as $t) {
      $t = mb_strtolower($t,'UTF-8');
      $where_search[] = "LOWER(name) LIKE '%{$t}%'";
    }
    $term = mb_strtolower($term,'UTF-8');
    $where = "AND (".implode(" AND ", $where_search).")";
  }

/*
  $q = db_query("SELECT count(*) as c FROM ".PRODUCT_MODELS. " WHERE bid = '{$bid}' {$where} ");
  $r = db_fetch_assoc($q);
  $count = $r['c'];
*/
  $q = db_query("SELECT DISTINCT * FROM ".PRODUCT_MODELS." WHERE bid = '{$bid}' {$where}  ORDER BY name ASC LIMIT {$start}, 100");
  while ($r = db_fetch_assoc($q)) {
    $models[] = array( 'value' => $r['name']);
  }
  return array('models' => $models, 'count' => $count, 'success' => true);
}

/**
 * @param $bid
 * @param int $re_compatibility
 * @return array
 */
function prdGetCount($bid, $re_compatibility = 0) {
  $where_bid = "";
  if ($bid > 0) {
    $brand = prdGetBrandFromId($bid);
    if (!empty($brand['cids'])) {
      $where_bid = "AND (categoryID in ({$brand['cids']}))";
    }
  }

/*  if ($re_compatibility) {
    $q = db_query("SELECT * FROM ".PRODUCTS_TABLE." ");
    while ($r = db_fetch_assoc($q)) {
      db_phquery("UPDATE ?#PRODUCTS_TABLE SET brief_description2_ru=? WHERE productID=?", $r['brief_description_ru'], $r['productID']);
    }
  }*/

  $count = array();
  $q = db_query("SELECT count(*) as c FROM ".PRODUCTS_TABLE. " WHERE 1 {$where_bid} ");
  $r = db_fetch_assoc($q);
  $count['do'] = $r['c'];

  $q = db_query("SELECT count(*) as c FROM ".PRODUCTS_TABLE. " WHERE 1 {$where_bid}");
  $r = db_fetch_assoc($q);
  $count['all'] = $r['c'];
  return $count;
};

/**
 * @param $series
 * @return array
 */
function prdSetSeries($series) {
  $series = explode(',', $series);

  $valuesArr = array();
  foreach($series as $_series){
    $_series = trim(mysql_real_escape_string( $_series ));

    if (!empty($_series)) {
      $valuesArr[] = "('$_series')";
    }
  }

  db_query("DELETE FROM ".PRODUCT_SERIES." WHERE 1");
  db_query("ALTER TABLE ".PRODUCT_SERIES." AUTO_INCREMENT = 1");

  if (!empty($valuesArr))
    db_query("INSERT IGNORE INTO ".PRODUCT_SERIES." (`name`) VALUES ".implode(',', $valuesArr));

  return array('success' => true);
};

/**
 * @return array
 */
function prdGetSeries() {
  $q = db_query("SELECT * FROM ".PRODUCT_SERIES. " WHERE 1");
  $result = array();
  while ( $r = db_fetch_assoc($q) ) {
    $result[] = $r['name'];
  }
  //$result = implode(', ', $result);
  return $result;
};

/**
 * @param $pid
 * @param $sub_slug
 * @return array
 */
function prdGetSubProduct($pid, $sub_slug) {
  $q = db_query("SELECT ps.*, psc.name AS name_custom, psc.noindex AS noindex_custom  FROM ".PRODUCTS_ITEMS. " AS pi
                  JOIN ".PRODUCTS_SUB." AS ps ON ps.iid=pi.iid
                  LEFT JOIN ".PRODUCTS_SUB_CUSTOM." AS psc ON psc.pid=pi.pid AND psc.item=pi.item
                  WHERE pi.pid = '{$pid}' AND pi.slug = '{$sub_slug}'");

  $r = db_fetch_assoc($q);
  return $r;
}

/**
 * @return mixed
 */
function prdSubSetting() {
  $q = db_query("SELECT * FROM ".TBL_CONFIG_SETTINGS." WHERE SettingName = 'additional_products' ") or die (db_error());
  $s = db_fetch_assoc($q);
  return $s['SettingValue'];
}

/**
 * @param $pid
 * @param $items
 * @return array
 */
function prdCreateItems($pid, $items) {
  $valuesArr = array();
  foreach($items as $item){
    $pid = (int) $pid;
    $slug = slug($item);
    $item = trim(mysql_real_escape_string( $item ));
    if (!empty($pid) && !empty($item)) {
      $valuesArr[] = "('$pid', '$item', '$slug')";
    }
  }

  if (!empty($valuesArr)) {
    db_query("INSERT IGNORE INTO ".PRODUCTS_ITEMS." (`pid`, `item`, `slug`) VALUES ".implode(',', $valuesArr));
  }

  $q = db_query("SELECT * FROM ".PRODUCTS_ITEMS." WHERE pid = '{$pid}'");
  $items = array();
  while ($item = db_fetch_assoc($q)) {
    $items[] = $item;
  }
  return $items;
}

/**
 * @param $str
 * @return mixed|string
 */
function slug($str) {
  $str = strtolower(trim($str));
  $str = preg_replace('/[^a-z0-9-]/', '-', $str);
  $str = preg_replace('/-+/', "-", $str);
  return $str;
}

/**
 * @param $items
 * @param $c
 */
function prdCreateSubs($items, $c) {
  $valuesArr = array();

  $sufix = ''; $prefix = '';
  if (!empty( $c['sufix']) && !empty($c['features_ru'])) {
    $sufix_arr = prdGenSufix($c['sufix'], $c['features_ru']);
    $sufix = implode(', ', $sufix_arr);
  }
  if (!empty($c['prefix'])) {
    $prefix = $c['prefix'];
  }

  foreach($items as $item){
    $iid = (int) $item['iid'];
    $item_name = trim(mysql_real_escape_string( $item['item'] ));
    $name = trim(mysql_real_escape_string( implode(' ', array($prefix, $item_name, $sufix)) ));
    $valuesArr[] = "('{$iid}', '$name')";
  }

  if (!empty($valuesArr)) {
    db_query("INSERT INTO ".PRODUCTS_SUB." (`iid`, `name`) VALUES ".implode(',', $valuesArr));
  }
}

/**
 * @param $items
 * @param $text
 * @return array
 */
function prdGenSufix($items, $text){
  $sufix_arr = array();
  $items = explode(',', $items);
  $text = strip_tags($text);
  $text = preg_replace('/&nbsp;/', ' ', $text);

  foreach ( $items as $item) {
    $item = trim($item);
    $tmp  = preg_split("/{$item}/i", $text);

    if (count($tmp) > 1) {
      $_text = preg_replace('/\n/', '|||', trim($tmp[1]));
      $_sufix = explode('|||', $_text);

      $sufix_arr[] = trim(preg_replace('/(\n+|\s+)/', ' ', $_sufix[0]));
    }
  }

  return $sufix_arr;
}

/**
 * @param $pid
 */
function prdClearSubsFromPID($pid) {
  db_query("DELETE FROM ".PRODUCTS_SUB." WHERE iid IN (
      SELECT iid FROM (
          SELECT iid FROM ".PRODUCTS_ITEMS." WHERE pid = '{$pid}') as `temp`
        )");
}

/**
 * @param $pid
 */
function prdClearItemsFromPID($pid) {
  db_query("DELETE FROM ".PRODUCTS_ITEMS." WHERE pid = '{$pid}'");
}

/**
 * @param $pid
 */
function prdUpdateNoIndexFromPID($pid){
  db_phquery(" UPDATE ".PRODUCTS_SUB." SET noindex =1 WHERE name IN (
    SELECT name FROM (
      SELECT ps.name AS name FROM ".PRODUCTS_ITEMS." AS pi
      JOIN ".PRODUCTS_SUB." AS ps ON pi.iid = ps.iid
      WHERE pi.pid = '{$pid}' GROUP BY ps.name HAVING COUNT( * ) >1 ) AS  `temp`
    )");
}

?>
