<?php

class quickOrderController extends ActionsController {

  public function order()
  {
    $Register = &Register::getInstance();
    $PostVars = &$Register->get(VAR_POST);
    $smarty = &$Register->get(VAR_SMARTY);

    $customer = Customer::getAuthedInstance();
    if ( !$customer )
    {
      $customer = new Customer;
      $customer->save();
    }

    $order_time = Time::dateTime();
    $customer_ip = stGetCustomerIP_Address();
    $customerID = $customer->customerID;

    $productID = intval($PostVars['productID']);
    $product_data = GetProduct($productID);

    $quantity = ( isset($_POST['Quantity']) && $_POST['Quantity'] > 0 ) ? $_POST['Quantity'] : 0;
    $variants = explode(',',$PostVars['variants']);
    $order_amount = GetPriceProductWithOption($variants, $productID);
    $_order_amount = $quantity*$order_amount;

    $phone = $PostVars['phone'];
    $name = $PostVars['name'];
    $com = $PostVars['comment'];
    $comment = "Быстрый заказ, тел. $phone.";// Контактное лицо: $name. Комментарий: $com";

    $q = "INSERT INTO SC_orders (`customerID`,`customer_ip`,`order_time`,`statusID`,`order_amount`,`currency_code`,`currency_value`,`customers_comment`,`customer_firstname`,`customer_lastname`,`shipping_firstname`,`shipping_lastname`) VALUES ($customerID,'$customer_ip','$order_time',2,$_order_amount,'RUR',1,'$comment','{$customer->first_name}','{$customer->last_name}','{$customer->first_name}','{$customer->last_name}')";
    mysql_query($q);
    $r = mysql_query("SELECT LAST_INSERT_ID() as id FROM SC_orders");
    if ( $r )
    {
      $row = mysql_fetch_assoc($r);
      $orderID = $row['id'];

      db_phquery('INSERT INTO SC_shopping_cart_items (`productID`) VALUES (?)',$productID);
      $r = mysql_query("SELECT LAST_INSERT_ID() as itemID FROM SC_shopping_cart_items");
      if ( $r )
      {
        $row = mysql_fetch_assoc($r);
        $itemID = $row['itemID'];


        $q = 'INSERT INTO SC_shopping_cart_items_content (`itemID`,`variantID`) VALUES (?,?)';
        if ( is_array($variants) )
          foreach ( $variants as $v )
            db_phquery($q,$itemID,$v);

        $options = trim( GetStrOptions($variants) );
        $options = ( !empty($options) ) ? ' ('.$options.')' : '';
        $productComplexName = $product_data['name_ru'].$options;

        $dbq = 'INSERT ?#ORDERED_CARTS_TABLE (itemID, orderID, name, Price, Quantity) VALUES (?, ?, ?, ?, ?)';
        db_phquery($dbq, $itemID, $orderID, $productComplexName, $order_amount, $quantity);

        $smarty->assign('order_amount',$order_amount);
        $smarty->assign('orderID',$orderID);
        $smarty->assign('success',1);

        global $smarty_mail;
        _sendOrderNotifycationToAdmin( $orderID, $smarty_mail, $tax );
      }
    }
  }

  public function main()
  {
    $Register = &Register::getInstance();
    $GetVars = &$Register->get(VAR_GET);
    $PostVars = &$Register->get(VAR_POST);
    $smarty = &$Register->get(VAR_SMARTY);

    $productID = intval($GetVars['productID']);
    $product_data = GetProduct($productID);

    $variants = array();
    foreach ( $GetVars as $k=>$v )
    {
      if ( !strstr($k, 'option_') ) continue;
      $variants[] = $v;
    }

    $options = trim( GetStrOptions($variants) );
    $options = ( !empty($options) ) ? ' ('.$options.')' : '';
    $productComplexName = $product_data['name_ru'].$options;

    $smarty->assign('price',GetPriceProductWithOption($variants, $productID));
    $smarty->assign('variants',implode(',',$variants));
    $smarty->assign('productID',$productID);
    $smarty->assign('product_name',$productComplexName);
  }

}

ActionsController::exec('quickOrderController');