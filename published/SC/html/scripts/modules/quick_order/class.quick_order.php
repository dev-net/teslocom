<?php

class quick_order extends ComponentModule {

  function initInterfaces()
  {
    $this->Interfaces = array(
      'quick_order' => array(
        'name' => 'Quick order',
        ),
    );

    $this->__registerComponent('qorder','Быстрый заказ',array(TPLID_GENERAL_LAYOUT,TPLID_PRODUCT_INFO));
  }

  function cpt_qorder()
  {
    $Register = &Register::getInstance();
    $smarty = &$Register->get(VAR_SMARTY);

    echo $smarty->fetch('quick_order_button.html');
  }

}