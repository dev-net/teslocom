<?php

  function _sortSetting( &$smarty, $urlToSort, $searchstring )
  {
    $sort_fields = array(
      array('name', 'NAME'),
      array('Price', 'PRICE'),
      array('customers_rating', 'RATING')
    );
    $sort_string = translate("prd_sort_main_control_string");
    $current_sort_field = isset($_GET['sort'])?$_GET['sort']:'';
    $current_sort_direction = isset($_GET['direction'])?$_GET['direction']:'';

    foreach ($sort_fields as $field){

      $sort_string = str_replace( "{ASC_".$field[1]."}", $field[0] == $current_sort_field && $current_sort_direction == 'ASC'?translate("str_ascending"):"<a href='".set_query("&sort={$field[0]}&direction=ASC&searchstring=".urlencode($searchstring))."'>".translate("str_ascending")."</a>",	$sort_string );
      $sort_string = str_replace( "{DESC_".$field[1]."}", $field[0] == $current_sort_field && $current_sort_direction == 'DESC'?translate("str_descending"):"<a href='".set_query("&sort={$field[0]}&direction=DESC&searchstring=".urlencode($searchstring))."'>".translate("str_descending")."</a>",	$sort_string );
    }
    $smarty->assign( "string_product_sort", $sort_string );
  }

  $Register = &Register::getInstance();
  /*@var $Register Register*/
  $smarty = &$Register->get(VAR_SMARTY);
  /*@var $smarty Smarty*/
  $GetVars = &$Register->get(VAR_GET);
  $PostVars = &$Register->get(VAR_POST);

  $searchstring = '';
  $searchbrand = '';
  $bid = '';
  $action = '';
  if (isset($GetVars['searchstring']))  $searchstring = urldecode($GetVars['searchstring']);
  if (isset($PostVars['searchstring'])) $searchstring =  urldecode($PostVars['searchstring']);
  if (isset($GetVars['searchbrand']))  $searchbrand = urldecode($GetVars['searchbrand']);
  if (isset($PostVars['searchbrand'])) $searchbrand =  urldecode($PostVars['searchbrand']);
  if (isset($GetVars['bid'])) $bid =  urldecode($GetVars['bid']);
  if (isset($PostVars['bid'])) $bid =  urldecode($PostVars['bid']);
  if (isset($GetVars['action'])) $action =  urldecode($GetVars['action']);
  if (isset($PostVars['action'])) $action =  urldecode($PostVars['action']);

  if (!empty($action)) {
    if ($action == 'search_empty') {

      $text = '';
      $contact = '';
      if (isset($GetVars['text']))    $text =  strip_tags(trim(urldecode($GetVars['text'])));
      if (isset($GetVars['contact'])) $contact =  strip_tags(trim(urldecode($GetVars['contact'])));

      if (!empty($text) && !empty($contact)) {
        $message = <<<HTML
          <strong>Текст сообщения:</strong> {$text} <br /><br />
          <strong>Контакты:</strong> {$contact}
HTML;

        ss_mail(CONF_ORDERS_EMAIL, "Покупатель не нашел нужный товар.", $message, true);
        echo json_encode(array('do'=>'success'));
      } else {
        echo json_encode(array('do'=>'error'));
      }
      die;
    }
  }

  $searchstrings = array();
  $tmp = explode(' ', $searchstring);
  foreach( $tmp as $key=> $val ){
    if ( strlen( trim($val) ) > 0 ) $searchstrings[] = $val;
  }

  /**
   * Ajax search autocomplete
   */
if ($action == 'autocomplete') {
    if (isset($GetVars['term'])) {
      $term = trim($GetVars['term']);
      if (!empty($term)) {
        $results = ajaxFindProducts($term);
        echo json_encode($results);
        die;
      }
    }
  }
  /**
   * Ajax search brand
   */
  if ($action == 'searchbrand') {
    if (!empty($bid)){
      $term = isset($GetVars['term']) ? trim($GetVars['term']) : "";
      $start = isset($GetVars['start']) ? intval($GetVars['start']) : 0;
      $results = ajaxFindModels($bid, $term, $start);
      echo json_encode($results);
      die;

    }
  }

  $in_results = false;
  if (isset($GetVars["in_results"]))  $in_results = true;
  if (isset($PostVars["in_results"])) $in_results = true;

  $smarty->assign("search_in_results", $in_results);

  if($in_results){
    $data = scanArrayKeysForID($PostVars, array('search_string'));
    foreach( $data as $key => $value ) $searchstrings[] = $value['search_string'];
  }

  $smarty->hassign( 'searchstrings', $searchstrings );
  $smarty->hassign( 'searchstring', $searchstring);
  $smarty->hassign( 'select_bid', $bid);
  $smarty->hassign( 'searchbrand', $searchbrand);


  $callBackParam	= array();
  $products	= array();
  $callBackParam['search_simple'] = $searchstrings;
  $callBackParam['search_tags'] = $searchstrings;
  $callBackParam['bid'] = $bid;
  $callBackParam['searchbrand'] = $searchbrand;

  if ( isset($GetVars['sort']) )$callBackParam['sort'] = $GetVars['sort'];
  if ( isset($GetVars['direction']) )$callBackParam['direction'] = $GetVars['direction'];

  $Register->assign("show_all", isset($GetVars['show_all']));
  renderURL('show_all=', '', true);
  $countTotal = 0;
  $navigatorHtml = GetNavigatorHtml('&searchstring='.urlencode($searchstring).'&searchbrand='.urlencode($searchbrand).'&bid='.intval($bid), CONF_PRODUCTS_PER_PAGE, 'prdSearchProductByTemplate', $callBackParam, $products, $offset, $countTotal );

  /**
  * @features "Products comparison"
  */
  if(CONF_ALLOW_COMPARISON_FOR_SIMPLE_SEARCH){
    $show_comparison = 0;
    foreach ($products as $_Key=>$_Product){
      $products[$_Key]['allow_products_comparison'] = 1;
      $show_comparison++;
    }
    $smarty->assign( 'show_comparison', $show_comparison>1 );
  }

  /**
  * @features
  */
  if ( CONF_PRODUCT_SORT == '1' )_sortSetting( $smarty, set_query(), $searchstring );

  $brands = prdBrands();
  $smarty->assign( 'brands',  $brands );
  $smarty->assign( 'products_to_show',  $products );
  $smarty->assign( 'products_found', $countTotal );
  $smarty->assign( 'products_to_show_count', $countTotal );
  $smarty->assign( 'search_navigator', $navigatorHtml );
  $smarty->assign( 'main_content_template', 'search_simple.html' );
?>