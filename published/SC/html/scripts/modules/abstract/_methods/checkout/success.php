<?php
class SuccessController extends ActionsController {
	
	function main(){
		$Register = &Register::getInstance();
		/*@var $Register Register*/
		$smarty = &$Register->get(VAR_SMARTY);
		/*@var $smarty Smarty*/
		$checkoutEntry = &Checkout::getInstance(_CHECKOUT_INSTANCE_NAME);
		
		if(!(isset($_GET["orderID"]) && isset($_SESSION["newoid"]) && (int)$_SESSION["newoid"] == (int)$_GET["orderID"]))
			RedirectSQ('?');
		
//start - uzayli 03.02.2011
		$gdeSlonURL = '<script type="text/javascript" src="http://www.gdeslon.ru/thanks.js?codes='.implode(',', $_SESSION['gids']).'&order_id='.$_SESSION["newoid"].'"></script>';
		$smarty->assign( 'gdeSlonURL', $gdeSlonURL  );
//end - uzayli 03.02.2011		

//quatrix modifications :: start

			$orderID=$_GET["orderID"];			
			$q_order=ordGetOrder($orderID);
			$smarty->assign( "q_order_amount",$q_order["order_amount"] );
			$smarty->assign( "q_order_id", $orderID);
//quatrix modifications :: fin
		$cartEntry = new ShoppingCart();
		$cartEntry->cleanCurrentCart();
		$paymentMethod = payGetPaymentMethodById($checkoutEntry->paymentMethodID());
		$currentPaymentModule = PaymentModule::getInstance($paymentMethod['module_id']);
	
		if ( $currentPaymentModule != null ){
			$after_processing_html = $currentPaymentModule->after_processing_html($_GET['orderID']);
		}else{
			$after_processing_html = '';
		}
		$smarty->assign( 'after_processing_html', $after_processing_html );

		$smarty->assign( 'order_success', 1 );
	
		$smarty->assign('main_content_template', 'checkout.success.html');
    
    // dec rest of product
    $prds = $_SESSION["cart"];
    if (!empty($prds))
      foreach ($prds as $prd)
        SLDRdecNumRestProduct(intval($prd['productID']), intval($prd['quantity']));
    
		//SLDRdecNumRestProduct($code, $quality = 0);

	}
}     

?>