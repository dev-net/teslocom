<?php

class price_wholesale extends ComponentModule {

  function initInterfaces()
  {
    $this->Interfaces = array(
      'price_wholesale' => array(
        'name' => 'Price wholesale',
        ),
    );
    
    $this->__registerComponent('pwholesale','Цена на опт',array(TPLID_GENERAL_LAYOUT,TPLID_PRODUCT_INFO));
  }

}