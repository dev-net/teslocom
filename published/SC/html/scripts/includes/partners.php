<?php

/**
 * @return bool
 */
function choose_partner() {
	if (isset($_POST['choose_partner'])) {
		$pid = intval($_POST['choose_partner']);
		if (!empty($pid)){
			$_SESSION['choose_p'] = $pid;
			return true;
		}
	}

	unset($_SESSION['choose_p']);
	return false;
}

/**
 *
 */
function clear_partner() {
	unset($_SESSION['choose_p']);
	return;
}

/**
 *
 */
function choose_products() {
	if (isset($_POST['product_type'])) {
		$pt = intval($_POST['product_type']);
		$_SESSION['pt'] = $pt;
		return true;
	}

	unset($_SESSION['pt']);
	return false;
}

if (isset($_POST['action'])){
	if ($_POST['action'] == 'choose_products'){
		choose_products();
	}

	if ($_POST['action'] == 'choose_partner'){
		choose_partner();
	}

	if ($_POST['action'] == 'clear_partner'){
		clear_partner();
	}
}

?>