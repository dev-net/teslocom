<?php 
/**
 * Include content for home slider
 */
 
 define('URL_SLIDERS', URL_PRODUCTS_PICTURES.'/../sliders');
 
 $actions = SLDRgetActionsActive();
 
 $smarty->assign( "URL_SLIDERS", URL_SLIDERS);
 
 if (!empty($actions)){
    $i = 0;
    if (count($actions) > 1){
      $i = count($actions);
      $i = rand(0, $i-1);
    }
    
    $curr_action = $actions[$i];    
    $images = SLDRgetImages($curr_action['id']);    
    $product = SLDRgetProduct($curr_action['product_code']);
    
    $smarty->assign('product', $product);          
    $smarty->assign( "images", $images );
    $smarty->assign( "action", $curr_action );
  }
  else {  
    $images_home = SLDRgetHomeImages();
    $smarty->assign( "images_home", $images_home );
  }
?>