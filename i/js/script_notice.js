	$(document).ready(function() {
		$("#formNotice").validate({ 
			debug: false, 
			errorClass: "invalid", 
			validClass: "success", 
			
			rules: { 
				ftitle: { 
					required: true, 
					maxlength: 200, 
					alphanumericRus: true
				}, 
				
				fname: { 
					maxlength: 100,
					alphanumericRus: true
				}, 
/*
				fmail: { 
					required: true,
					email: true,
					maxlength: 50
				}, 
*/
				fpass: {
					required: true,
					maxlength: 10
				}, 
				ftext: {
					required: true,
					maxlength: 500
				} 
			}, 
			messages: { 
				ftitle: { 
					required: '����������� ��� ����������', 
					maxlength: '������������ ����� 200 ��������', 
					alphanumericRus: '��������� �����, ����� � �����'
				}, 
				fname: { 
					maxlength: '������������ ����� 100 ��������',
					alphanumericRus: '��������� �����, ����� � �����'
				}, 
/*
				fmail: { 
					required: '����������� ��� ����������',
					email: '������: picom@mail.ru',
					maxlength: '������������ ����� 50 ��������'
				}, 
*/
				fpass: {
					required: '����������� ��� ����������',
					maxlength: '������������ ����� 10 ��������'
				}, 
				ftext: {
					required: '����������� ��� ����������',
					maxlength: '������������ ����� 500 ��������'
				} 
			}
			
		})
	});
